﻿// Reference library: https://products.fileformat.com/spreadsheet/net/fastexcel/?ref=hackernoon.com
//                    https://github.com/ahmedwalid05/FastExcel
using System;
using DataGenerator;
using FastExcel;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Data;
using System.Diagnostics;
using System.Text.Json;
using System.Reflection.Metadata.Ecma335;
using System.Diagnostics.CodeAnalysis;

// Dictionary of Values Catalog
ConcurrentBag<ColumnSetting> listCatalogsColumns = new ConcurrentBag<ColumnSetting>();
const string pathDataConfig = @"Resources\dataconfig.xlsx";
const string pathEmptyTemplate = @"Resources\Templates\empty.xlsx";
const string pathTemplate = @"Resources\Templates\template.xlsx";
const int TOTAL_OF_ROWS = 100000;
string pathOutput = @"Resources\Outputs";
string nameOutputFile = string.Concat("data_", DateTime.Now.ToString("yyyyMMddTHHmmss"), ".xlsx");

try
{
    // Read the file input data.config
    readDataFileConfig();

    // Write the file output with data (*.xlsx)
    dataGeneratorList();
}
catch (Exception ex)
{
    Console.WriteLine(ex.ToString());
}

void readDataFileConfig()
{
    deleteFileExists();

    using (FastExcel.FastExcel manageFileXls = new FastExcel.FastExcel(new FileInfo(pathDataConfig), true))
    {

        // Read CONFIG sheet
        Worksheet sheet = manageFileXls.Read("config");
        Stopwatch stopwatch = Stopwatch.StartNew();

        // Read all rows configuration
        sheet.Read();
        bool isHeader = true;

        List<ColumnSetting> sortList = new List<ColumnSetting>();
        sheet.Rows.ToList().ForEach(row =>
        {
            if (!isHeader)
            {
                ColumnSetting dataSetting = new ColumnSetting();
                dataSetting.Catalog = Convert.ToString(row.GetCellByColumnName("A").Value);
                dataSetting.CatalogKey = Convert.ToString(row.GetCellByColumnName("B").Value);
                dataSetting.ColumnName = Convert.ToString(row.GetCellByColumnName("C").Value);
                dataSetting.ColumnID = Convert.ToString(row.GetCellByColumnName("D").Value);
                dataSetting.Order = Convert.ToInt32(row.GetCellByColumnName("E").Value);
                sortList.Add(dataSetting);
            }
            else
            {
                isHeader = false;
            }
        });

        // Sort columns settings ascendandly
        sortList = sortList.OrderBy(x => x.Order).ToList();
        for (int index = sortList.Count - 1; index >= 0; index--)
        {
            listCatalogsColumns.Add(sortList.ElementAt(index));
        }

        // Fill values for each CATALOG
        foreach (ColumnSetting columnSetting in listCatalogsColumns)
        {
            // Get the name sheet in file Xls.
            sheet = manageFileXls.Read(columnSetting.Catalog);

            // Define if catalog have one column whit the first row.
            columnSetting.isMultipleColumns = sheet.Rows.ElementAt(0).Cells.Count() > 1;

            columnSetting.DataByKey = new Dictionary<string, string>();

            // Fill values in DataByKey.
            sheet.Rows.ToList().ForEach(row =>
            {
                // When sheet contains one column.
                if (!columnSetting.isMultipleColumns)
                {
                    columnSetting.DataByKey.Add(Convert.ToString(row.RowNumber), Convert.ToString(row.GetCellByColumnName(columnSetting.ColumnID).Value));
                }
                else
                {
                    // When sheet contains multiple columns use catalog key column based in setting.
                    columnSetting.DataByKey.Add(Convert.ToString(row.GetCellByColumnName(columnSetting.CatalogKey).Value), Convert.ToString(row.GetCellByColumnName(columnSetting.ColumnID).Value));
                }
            });
        }
        Console.WriteLine("Time to read {0}", stopwatch.Elapsed);
    }
}

void generateTemplateFile()
{
    Stopwatch stopwatch = new Stopwatch();
    stopwatch.Start();

    if (!File.Exists(pathTemplate))
    {
        using (FastExcel.FastExcel manageTemplate = new FastExcel.FastExcel(new FileInfo(pathEmptyTemplate), new FileInfo(pathTemplate)))
        {
            object[] headers = new object[listCatalogsColumns.Count];
            List<Row> rows = new List<Row>();
            List<ColumnSetting> catalogs = listCatalogsColumns.OrderBy(x => x.Order).ToList();
            Worksheet worksheet = new Worksheet();

            for (int index = 0, cell = 1; index < catalogs.Count; cell++, index++)
            {
                headers[index] = catalogs[index].ColumnName;
            };
            worksheet.AddRow(headers);
            manageTemplate.Write(worksheet, "data");
        }
    }

    stopwatch.Stop();
    Console.WriteLine("File template took {0} seconds to generate.", stopwatch.Elapsed.Seconds);
}

void exportDataFile(ref List<Row> dataRows)
{
    Stopwatch stopwatch = new Stopwatch();
    stopwatch.Start();
    using (FastExcel.FastExcel fastExcel = new FastExcel.FastExcel(new FileInfo(pathTemplate), new FileInfo(pathOutput)))
    {
        Worksheet worksheet = new Worksheet();
        worksheet.Rows = dataRows;

        // Indicating that exist heading rows in template file, with 1 value in third parameter.
        fastExcel.Write(worksheet, "data", 1);
    }
    stopwatch.Stop();
    Console.WriteLine("Write data in output file took {0} seconds.", stopwatch.Elapsed.Seconds);
}


void getRandomItemInCatalog(out string textValue, in ColumnSetting catalogItem, in string colUniqueKey, in Dictionary<string, string> tempKeyValueMultipleColumns)
{
    int indexRandom = new Random().Next(1, catalogItem.DataByKey.Count());
    KeyValuePair<string, string> dataKey = catalogItem.DataByKey.ToList()[indexRandom];
    textValue = dataKey.Value;

    if (!tempKeyValueMultipleColumns.ContainsKey(colUniqueKey))
    {
        if(catalogItem.isMultipleColumns)
            tempKeyValueMultipleColumns.Add(colUniqueKey, textValue);
        else
            tempKeyValueMultipleColumns.Add(colUniqueKey, dataKey.Key);
    }
}

void dataGeneratorList()
{
    Stopwatch stopwatch = new Stopwatch();
    List<Row> dataRows = new List<Row>();
    Dictionary<string, string> tempKeyValueMultipleColumns;
    
    stopwatch.Start();

    // Generate rows with configurated columns.
    for (int i = 2; i <= TOTAL_OF_ROWS + 1; i++)
    {
        List<Cell> cellList = new List<Cell>();
        tempKeyValueMultipleColumns = new Dictionary<string, string>();

        listCatalogsColumns.ToList().ForEach(catalogItem =>
        {
            string textValue = string.Empty;
            string colUniqueKey = catalogItem.getCatalogUniqueKey();

            // When catalog sheet cointains multiple columns, 
            if (catalogItem.isMultipleColumns)
            {
                if (tempKeyValueMultipleColumns.ContainsKey(colUniqueKey))
                {
                    string keyTextValue = tempKeyValueMultipleColumns[colUniqueKey];
                    textValue = catalogItem.DataByKey[keyTextValue];
                }
                else
                {
                    getRandomItemInCatalog(out textValue, catalogItem, colUniqueKey, tempKeyValueMultipleColumns);
                }
            }
            else
            {
                getRandomItemInCatalog(out textValue, catalogItem, colUniqueKey, tempKeyValueMultipleColumns);
            }

            // Save de column key generated
            cellList.Add(new Cell(catalogItem.Order, textValue));
        }
        );
        dataRows.Add(new Row(i, cellList));
    }

    stopwatch.Stop();
    Console.WriteLine("Data generator took {0} seconds", stopwatch.Elapsed.TotalSeconds);
    Console.WriteLine("Row list with {0} items.", dataRows.Count);

    #region Export Data
    generateTemplateFile();
    exportDataFile(ref dataRows);
    #endregion
}

void deleteFileExists()
{
    pathOutput = Path.Combine(pathOutput, nameOutputFile);

    if (File.Exists(pathOutput))
        File.Delete(pathOutput);

    if (File.Exists(pathTemplate))
        File.Delete(pathTemplate);
}