﻿using FastExcel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGenerator
{

    public class ColumnSetting
    {
        public string Catalog { get; set; }
        public string CatalogKey { get; set; }
        public string ColumnName { get; set; }
        public string ColumnID { get; set; }
        public int Order { get; set; }
        public bool isMultipleColumns { get; set; }
        public Dictionary<string, string> DataByKey { get; set; }
        public string getCatalogUniqueKey()
        {
            return string.Concat(Catalog, ":", CatalogKey);
        }

    }
}
